use async_std::io;
use async_std::path::Path;

#[async_std::main]
async fn main() {
    println!("Wow!");
}

async fn read_to_string(path: impl AsRef<Path>) -> io::Result<String> {
    std::fs::read_to_string(path)
}
